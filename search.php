<?php
    $ini_array = parse_ini_file("./config.ini");

    $servername = $ini_array["DB_HOST"];
    $username = $ini_array["DB_USER"];
    $password = $ini_array["DB_PASS"];
    $database = $ini_array["DB_NAME"];

    
    $conn = mysqli_connect($servername, $username, $password, $database);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    echo'<table><thead><tr><td> <font size=5%>Name</font> </td><td> <font size=5%>Preis</font> </td></tr></thead>';
   
    $lastTimestamp=$conn->query("SELECT MAX(stamp) FROM prices;");
    $lastTimestamp = $lastTimestamp->fetch_assoc();
    $lastTimestamp=$lastTimestamp["MAX(stamp)"];

    $searchBar= $_POST['searchBar'];

    $stattrack= $_POST['StatTrack'];
    $sticker= $_POST['Sticker'];
    $souvenir= $_POST['Souvenir'];
    $graffiti= $_POST['Graffiti'];

    $consumer= $_POST['Consumer'];
    $milspec= $_POST['MilSpec'];
    $industrial= $_POST['Industrial'];
    $restricted= $_POST['Restricted'];
    $classified= $_POST['Classified'];
    $covert= $_POST['Covert'];
    $base= $_POST['Base'];
    $high= $_POST['High'];
    $extraordinary= $_POST['Extraordinary'];
    $remarkable= $_POST['Remarkable'];
    $exotic= $_POST['Exotic'];
    $contraband= $_POST['Contraband'];

    $factorynew= $_POST['FactoryNew'];
    $minimalwear= $_POST['MinimalWear'];
    $fieldtested= $_POST['FieldTested'];
    $wellworn= $_POST['WellWorn'];
    $battlescarred= $_POST['BattleScarred'];

    $sortieren = $_POST['Sortieren'];

    $sql = "SELECT DISTINCT p.id, p.name, p.price, p.amountInStock, p.stamp, t.type FROM prices AS p inner join type as t on p.type=t.id WHERE p.stamp=".$lastTimestamp;

    if($searchBar!="''")
        $sql = $sql." AND p.name REGEXP ".$searchBar;

    $sql = $sql." AND";

    if($stattrack=="true")
        $sql = $sql." t.type REGEXP 'StatTrak' OR";
    if($sticker=="true")
        $sql = $sql." t.type REGEXP 'Sticker' OR";
    if($souvenir=="true")
        $sql = $sql." t.type REGEXP 'Souvenir' OR";
    if($graffiti=="true")
        $sql = $sql." t.type REGEXP 'Graffiti' OR";

    $sql = $sql." 0=1 AND";

    if($consumer=="true")
        $sql = $sql." t.type REGEXP 'Consumer' OR";
    if($milspec=="true")
        $sql = $sql." t.type REGEXP 'MilSpec' OR";
    if($industrial=="true")
        $sql = $sql." t.type REGEXP 'Industrial' OR";
    if($restricted=="true")
        $sql = $sql." t.type REGEXP 'Restricted' OR";
    if($classified=="true")
        $sql = $sql." t.type REGEXP 'Classified' OR";
    if($covert=="true")
        $sql = $sql." t.type REGEXP 'Covert' OR";
    if($base=="true")
        $sql = $sql." t.type REGEXP 'Base' OR";
    if($high=="true")
        $sql = $sql." t.type REGEXP 'High' OR";
    if($extraordinary=="true")
        $sql = $sql." t.type REGEXP 'Extraordinary' OR";
    if($remarkable=="true")
        $sql = $sql." t.type REGEXP 'Remarkable' OR";
    if($exotic=="true")
        $sql = $sql." t.type REGEXP 'Exotic' OR";
    if($contraband=="true")
        $sql = $sql." t.type REGEXP 'Contraband' OR";

    $sql = $sql." 0=1 AND";

    if($factorynew=="true")
        $sql = $sql." p.name REGEXP 'Factory New' OR";
    if($minimalwear=="true")
        $sql = $sql." p.name REGEXP 'Minimal Wear' OR";
    if($fieldtested=="true")
        $sql = $sql." p.name REGEXP 'FieldTested' OR";
    if($wellworn=="true")
        $sql = $sql." p.name REGEXP 'WellWorn' OR";
    if($battlescarred=="true")
        $sql = $sql." p.name REGEXP 'BattleScarred' OR";
    
    $sql = $sql." 0=1";

    if($sortieren=='PreisASC')
        $sql = $sql." ORDER BY p.price ASC";
    elseif($sortieren=='PreisDESC')
        $sql = $sql." ORDER BY p.price DESC";
    elseif($sortieren=='AlphabetASC')
        $sql = $sql." ORDER BY p.name ASC";
    elseif($sortieren=='AlphabetDESC')
        $sql = $sql." ORDER BY p.name DESC";
    elseif($sortieren=='Typ')
        $sql = $sql." ORDER BY t.type";

    $sql = $sql.";";
    
    $result = $conn->query($sql);
    if (!$result) {
        trigger_error('Invalid query: ' . $conn->error);
    }
    if($result->num_rows > 0) {
        echo $result->num_rows," results";
    
        while($row = $result->fetch_assoc()) {
            echo '<tr><td><a href="./articleView.php?name=' , rawurlencode($row["name"]) , '&price=', rawurlencode($row["price"]), '">' , $row["name"], ' </a></td><td> ' , $row["price"], '</td></tr>';
        }
    } else
        echo "0 results";
    echo '</table>';
    $conn->close();
    return "test";

?>
