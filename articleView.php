<html>
<head>
<link rel="stylesheet" type="text/css" href="./Site.css">
</head>
<body> 
<?php
    $ini_array = parse_ini_file("./config.ini");

    $servername = $ini_array["DB_HOST"];
    $username = $ini_array["DB_USER"];
    $password = $ini_array["DB_PASS"];
    $database = $ini_array["DB_NAME"];

    
    $conn = mysqli_connect($servername, $username, $password, $database);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $url = $_SERVER['REQUEST_URI'];
    $parts = parse_url($url);
    parse_str($parts['query'], $URLParameters);

    
?>

<div style="width:78%; float:right; margin-right:1%;">
    <font size=7%><?php echo $URLParameters['name'] ?></font>
    <hr/>
    <font size=5%> Preis: <?php echo $URLParameters['price'] ?> €</font></br>
    <font size=5%> Typ: <?php
    $sql='select t.type from prices as p inner join type as t on p.type=t.id where p.name="'.$URLParameters['name'].'" limit 1;';
        $result = $conn->query($sql);

        if (!$result) {
            trigger_error('Invalid query: ' . $conn->error);
        }
        if($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                echo $row["type"];
            }
        }
    ?> </font>
</div>
<div>
<img src="https://steamcommunity-a.akamaihd.net/economy/image/<?php
    $sql='select iu.icon_url from prices as p inner join icon_url as iu on p.icon_url=iu.id where p.name="'.$URLParameters['name'].'" limit 1;';
    $result = $conn->query($sql);

    if (!$result) {
        trigger_error('Invalid query: ' . $conn->error);
    }
    if($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            echo $row["icon_url"];
        }
    }
?>" width="20%">
</div>
<hr style="margin-left:1%; margin-right:1%;"/>

<?php
    $command = escapeshellcmd('python3 ./createPriceGraph.py "'.$URLParameters['name'].'"');
    $base64Graph = substr(shell_exec($command),2,-2);
    echo '<img src="data:image/gif;base64,'.$base64Graph.'" width="600"/>';

    $command = escapeshellcmd('python3 ./createAmountInStockGraph.py "'.$URLParameters['name'].'"');
    $base64Graph = substr(shell_exec($command),2,-2);
    echo '<img src="data:image/gif;base64,'.$base64Graph.'" width="600"/>';
?>
<hr style="margin-left:1%; margin-right:1%;"/>
<a href="./trading.php"> zurück zur Hauptseite </a>
</body>
</html>
