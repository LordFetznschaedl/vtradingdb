# vTradingDB   
## Virtual Trading Database zur Beobachtung von Preisveränderung von Virtuellen Gegenständen.

Verwendet werden die Programmiersprache Python unter Version 3.6 und PHP unter Version 7.2 mit Hilfe von HTML5 zur Websitendarstellung.   
Das verwendete Datenbankverwaltungssystem ist MySQL unter Version 14.14   
Als Webserver verwenden wir den im Ubuntu-php-Package integrierten Webserver welchen man mit folgendem Befehl startet.   
 >`php -S localhost:8000`  
 
Um diesen erfolgreich mit unserem Programm zu verwenden, muss man die Extension mysqli zur php.ini Datei hinzufügen.

Das Python-Skript wird laufend ausgeführt und bezieht die Preisdaten aus einer JSON-Datei aus dem Internet welche es in einer MySQL-Datenbank abspeichert.   
Dieses Skript erstellt ebenso die grafische Darstellung des Preisverlaufs mit Hilfe der Library matplotlib unter Version 2.2.2.   
Wenn man die Daten dargestellt haben will, öffnet man die Website auf welcher die verschiedenen Daten dargestellt werden.   

## Meilensteine Samuel Flatscher
 
 + **Aufsetzen der Datenbank (17.Jan.2019)** (DONE)   
 
 + **Python-Skript zur regelmäßigen Datenabfrage und Abspeichern in der Datenbank mit einem aktuellen Timestamp (31.Jan.2019)** (DONE)   
 
 + *Website für die Darstellung der Diagramme (31.Jan.2019)* (DONE)   
 
 + *Websitenerweiterung zur Abfrage von einzelnen bestimmten Gegenständen (14.Mar.2019)* (DONE) 
 
 + *Implementierung von Bildern / Websitenerweiterung (21.Feb.2019)* (DONE)
 
 + *mehr Filtereinstellungen auf Website (28.Feb.2019)* (DONE)
 
## Meilensteine Robert Blum

 + **Setup Python Script für die Datenbank (31. Jan. 2019)** (DONE)   

 + **Chartdarstellung für die Preise von einzelnen Gegenständen (7.Feb.2019)** (DONE)  
 
 + *URL von Bildern zu jedem Gegenstand (21.Feb.2019)* (DONE)
 
 + *Änderung der Preis API auf neuere und bessere Version (21.Feb.2019)* (DONE)
 
 + *Chartdarstellung für die Anzahl an Gegenständen im Markt (21.Feb.2019)* (DONE)