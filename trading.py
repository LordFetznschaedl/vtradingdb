import json
import mysql.connector
import urllib.request as urllib
import re
import time
import configparser
import os

os.chdir(str(os.path.dirname(os.path.abspath(__file__))))
config = configparser.ConfigParser()
config.read('config.ini')

mydb = mysql.connector.connect(
                               host=config.get('main', 'DB_HOST'),
                               user=config.get('main', 'DB_USER'),
                               passwd=config.get('main', 'DB_PASS'),
                               database=config.get('main', 'DB_NAME'),
                               buffered=True
                               )

mycursor = mydb.cursor()

#url='https://steamcommunity.com/market/search/render/?search_descriptions=default&sort_column=all&sort_dir=desc&appid=730&norender=1&count=100000000&start=0'
#req = urllib.Request(url, headers={'User-Agent' : "Magic Browser"})
#j = urllib.urlopen(req)
#jsondata=json.loads(j.read())
#data = jsondata["results"]
#
#c=0
#for x in data:
#    print(str(c) + " --- " + str(x["name"]) + " - " + str(x["sell_listings"]) + " - " + str(x["sell_price"]/100) + " - " + x["asset_description"]["icon_url"] + " - " + x["asset_description"]["type"])
#    c=c+1

dictList=[]
tempDictList = []
regex = re.compile('[^a-zA-Z0-9 | _ - . ]')
regex2 = re.compile('[^a-zA-Z0-9 | _ . ]')

itemcounter = 0
requestcounter = 0
start = 0
isNotEmpty = True

while isNotEmpty:
    url="https://steamcommunity.com/market/search/render/?search_descriptions=default&sort_column=all&sort_dir=desc&appid=730&norender=1&count=100000000&start="+str(start)
    req = urllib.Request(url, headers={'User-Agent' : "Magic Browser"})
    j = urllib.urlopen(req)
    jsondata=json.loads(j.read())
    data = jsondata["results"]
    
    for x in data:
        itemcounter = itemcounter + 1
        #print(str(itemcounter) + " --- " + str(x["name"]) + " - " + str(x["sell_listings"]) + " - " + str(x["sell_price"]/100) + " - " + x["asset_description"]["icon_url"] + " - " + x["asset_description"]["type"])
        tempDictList.append(x["name"])
        tempDictList.append(x["sell_listings"])
        tempDictList.append(x["sell_price"]/100)
        tempDictList.append(x["asset_description"]["icon_url"])
        tempDictList.append(x["asset_description"]["type"])

    time.sleep(11)

    start = start + 100;
    requestcounter = requestcounter + 1
    
    if(requestcounter == 24):
        requestcounter = 0
        print("SLEEP 180!")
        time.sleep(180)

    if(len(data) == 0):
        isNotEmpty = False
        print("DATA IS EMPTY")


for x in range(0,len(tempDictList),5):
    dictList.append(re.sub(regex2,'',tempDictList[x]).lstrip())
    dictList.append(tempDictList[x+1])
    dictList.append(tempDictList[x+2])
    dictList.append(tempDictList[x+3])
    dictList.append(re.sub(regex,'',tempDictList[x+4]).lstrip())


#print("--------------------------------")

itemcounter = 0
for i in range(0,len(dictList),5):
    itemcounter = itemcounter + 1
#    print(str(itemcounter) + " --- " + str(dictList[i]) + " - " + str(dictList[i+1]) + " - " + str(dictList[i+2]) + " - " + dictList[i+3] + " - " + dictList[i+4])


#STAMP
cmd='INSERT INTO timestamps(stamp) VALUES ("'+time.strftime('%Y-%m-%d %H:%M:%S')+'");'
mycursor.execute(cmd)
stamp = mycursor.lastrowid

for i in range(0, len(dictList),5):
    #ICON_URL
    cmd='INSERT IGNORE INTO icon_url (icon_url) VALUES ("'+dictList[i+3]+'");'
    mycursor.execute(cmd)
    
    cmd='SELECT id FROM icon_url WHERE icon_url="'+dictList[i+3]+'";'
    mycursor.execute(cmd)
    icon_urlID = mycursor.fetchone()
    
    #TYPE
    cmd='INSERT IGNORE INTO type (type) VALUES ("'+dictList[i+4].lstrip()+'");'
    mycursor.execute(cmd)
    
    cmd='SELECT id FROM type WHERE type="'+dictList[i+4]+'";'
    mycursor.execute(cmd)
    typeID = mycursor.fetchone()
    
    #PRICES
    cmd='INSERT INTO prices (name, price, amountInStock, stamp, type, icon_url) VALUES ("'+ str(dictList[i]).lstrip() +'", '+ str(dictList[i+2]) +', '+ str(dictList[i+1]) +', '+ str(stamp) +', '+ str(typeID[0]) +', '+ str(icon_urlID[0]) +');'
    mycursor.execute(cmd)



mydb.commit()
