#!/usr/bin/python

import sys
import mysql.connector
from mysql.connector import errorcode
import configparser
import os

os.chdir(str(os.path.dirname(os.path.abspath(__file__))))
config = configparser.ConfigParser()
config.read('config.ini')

connection = None
datenbankname = config.get('main', 'DB_NAME')
user = config.get('main', 'DB_USER')
password = config.get('main', 'DB_PASS')
host = config.get('main', 'DB_HOST')

TABLES = {}
TABLES['timestamps'] = (
                        "CREATE TABLE timestamps("+
                        "id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT, "+
                        "stamp DATETIME UNIQUE NOT NULL);")

TABLES['type'] = (
                        "CREATE TABLE type("+
                        "id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT, "+
                        "type VARCHAR(512) UNIQUE  NOT NULL);")

TABLES['icon_url'] = (
                  "CREATE TABLE icon_url("+
                  "id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT, "+
                  "icon_url VARCHAR(512) UNIQUE NOT NULL);")

TABLES['prices'] = (
                     "CREATE TABLE prices("+
                        "id INTEGER PRIMARY KEY AUTO_INCREMENT, "+
                        "name VARCHAR(512) NOT NULL,"+
                        "price REAL NOT NULL,"+
                        "amountInStock INTEGER NOT NULL,"+
                        "stamp INTEGER NOT NULL,"+
                        "type INTEGER NOT NULL,"+
                        "icon_url INTEGER NOT NULL,"+
                        "FOREIGN KEY(type) REFERENCES type(id),"+
                        "FOREIGN KEY(icon_url) REFERENCES icon_url(id),"+
                        "FOREIGN KEY(stamp) REFERENCES timestamps(id));")

def createDatabase(databaseName):
    cursor = connection.cursor()
    cursor.execute("CREATE DATABASE {0}".format(databaseName))
    cursor.close()

def createTable(createStatement):
    cursor = connection.cursor()
    cursor.execute(createStatement)
    cursor.close()


print("DATENBANK SETUP")

try:
    print("Verbindung zu Datenbankserver wird aufgebaut")
    connection = mysql.connector.connect(user=user, password=password, host=host)
    print("Verbindung aufgebaut")
    print("Verbindung zu Datenbank "+datenbankname+" wird aufgebaut")
    createDatabase(datenbankname)
    connection.database = datenbankname
    print("Verbindung hergestellt")

except mysql.connector.Error as e:
    if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("LOGINDATEN SIND FALSCH")
    elif e.errno == errorcode.ER_BAD_DB_ERROR:
        createDatabase(datenbankname)
        print("DATENBANK ERSTELLT")
        connection.database = datenbankname
        print("Verbindung hergestellt")
    else:
        print(str(e))
except:
    print("ERROR: "), sys.exc_info()[0]

for name, ddl in TABLES.items():
    try:
        print("Erstelle Tabelle {0}: ".format(name))
        createTable(ddl)
    except mysql.connector.Error as e:
        if e.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("TABELLE EXISTIERT SCHON")
        else:
            print(str(e))
    except:
        print("ERROR: "), sys.exc_info()[0]
    else:
        print("Erstellt!")

connection.commit()

connection.close()

sys.exit(0)

