#!/usr/bin/env python3

import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib.gridspec as gridspec
import configparser
import os
import mysql.connector
from dateutil import parser
import time
import matplotlib.dates as mdates
from io import BytesIO
import base64


os.chdir(str(os.path.dirname(os.path.abspath(__file__))))
config = configparser.ConfigParser()
config.read('config.ini')

mydb = mysql.connector.connect(
                               host=config.get('main', 'DB_HOST'),
                               user=config.get('main', 'DB_USER'),
                               passwd=config.get('main', 'DB_PASS'),
                               database=config.get('main', 'DB_NAME'),
                               buffered=True
                               )

mycursor = mydb.cursor()

cmd = "select p.price, ts.stamp from prices as p inner join timestamps as ts on p.stamp=ts.id where p.name=\""+sys.argv[1]+"\";"

prices = []
stamps = []

try:
    mycursor.execute(cmd)
    results = mycursor.fetchall()
    for row in results:
        prices.append(row[0])
        stamps.append(row[1])
#print(row[1])


except:
    print ("Error: unable to fetch data")


fig = plt.figure(figsize=(8, 6))
ax = plt.subplot()
ax.plot(stamps,prices, marker="x")
plt.xticks(rotation=90)

#ax.set(xlabel='DATE', ylabel='PRICE $', title='PRICE HISTORY')
plt.xlabel('DATE',)
plt.ylabel('PRICE')
plt.title('PRICE HISTORY')
plt.subplots_adjust(left=0.12, right=0.95, bottom=0.30, top=0.9)
ax.xaxis.set_major_formatter(mdates.DateFormatter("%d-%m-%Y %H %p"))
ax.xaxis.set_minor_formatter(mdates.DateFormatter("%d-%m-%Y %H %p"))
ax.grid()

#fig.savefig("test.png")


figfile = BytesIO()
plt.savefig(figfile, format='png')
figfile.seek(0)
figdata_png = figfile.getvalue()
figdata_png = base64.b64encode(figdata_png)

print(figdata_png)

#select p.name, p.price, ts.stamp from prices as p inner join timestamps as ts on p.stamp=ts.id where p.name="?";
