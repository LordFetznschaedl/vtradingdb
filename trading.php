<html>
<head>
<link rel="stylesheet" type="text/css" href="./Site.css">
<style>
.dropdown {
  position: relative;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: LIGHTGREY;
  width: 600px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  padding: 12px 16px;
  margin:3px 0;
  z-index: 1;
}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>
</head>
<body style="margin:1%;">
<form id="searchForm" onsubmit="search()">
    <input type="text" id="searchBar" value="" style="width:40%;margin:3px;"><input type="button" onclick="search()" value="Search" style="width:10%;"></br>
    <div class="dropdown" style="width:10%;margin:3px;">
        <div style="background:lightgrey;">
            <font size=5% color="white" style="align:center;">Filter</font>
        </div>
       <div class="dropdown-content">
            <table>
                <tr>
                    <td><font size=4% color="white">Kategorie</font></td>
                    <td><font size=4% color="white">Abnutzung</font></td>
                    <td><font size=4% color="white">Qualität</font></td>   
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" id="StatTrack" checked>StatTrack </br>
                        <input type="checkbox" id="Sticker" checked>Sticker </br>
                        <input type="checkbox" id="Souvenir" checked>Souvenir</br>
                        <input type="checkbox" id="Graffiti" checked>Graffiti
                    </td>
                    <td>
                        <input type="checkbox" id="FactoryNew" checked>Factory New </br>
                        <input type="checkbox" id="MinimalWear" checked>Minimal Wear </br>
                        <input type="checkbox" id="FieldTested" checked>FieldTested</br>
                        <input type="checkbox" id="WellWorn" checked>WellWorn</br>
                        <input type="checkbox" id="BattleScarred" checked>BattleScarred
                    </td>
                    <td>
                        <input type="checkbox" id="Consumer" checked>Consumer Grade</br>
                        <input type="checkbox" id="MilSpec" checked>MilSpec Grade</br>
                        <input type="checkbox" id="Industrial" checked>Industrial Grade</br>
                        <input type="checkbox" id="Restricted" checked>Restricted</br>
                        <input type="checkbox" id="Classified" checked>Classified</br>
                        <input type="checkbox" id="Covert" checked>Covert</br>
                        <input type="checkbox" id="Base" checked>Base Grade</br>
                        <input type="checkbox" id="High" checked>High Grade</br>
                        <input type="checkbox" id="Extraordinary" checked>Extraordinary</br>
                        <input type="checkbox" id="Remarkable" checked>Remarkable</br>
                        <input type="checkbox" id="Exotic" checked>Exotic</br>
                        <input type="checkbox" id="Contraband" checked>Contraband
                    </td>
                </tr>   
            </table>    
        </div>  
    </div>
    <div class="dropdown" style="width:10%;margin:3px;">
        <div style="background:lightgrey;">
            <font size=5% color="white" style="align:center;">Sortieren</font>
        </div>
        <div class="dropdown-content">
            <fieldset>
                <input type="radio" id="PreisASC" name="Sortieren" value="PreisASC">
	    		<label for="PreisASC"> Preis - aufsteigend</label></br>
		    	<input type="radio" id="PreisDESC" name="Sortieren" value="PreisDESC">
		    	<label for="PreisDESC"> Preis - absteigend</label></br>
		      	<input type="radio" id="AlphabetASC" name="Sortieren" value="AlphabetASC">
			    <label for="AlphabetASC"> Alphabetisch - A bis Z</label></br>
		      	<input type="radio" id="AlphabetDESC" name="Sortieren" value="AlphabetDESC">
			    <label for="AlphabetDESC"> Alphabetisch - Z bis A</label></br>
		      	<input type="radio" id="Typ" name="Sortieren" value="Typ">
			    <label for="Typ"> nach Typ</label></br>
            </fieldset>
        </div>  
    </div>
</form>

<div id="ausgabe"></div>

<script>
function search(){
    var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("ausgabe").innerHTML = this.responseText;
    	}
	};
	xhttp.open("POST", "./search.php",true);
	xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");

    var data="searchBar='"+document.getElementById("searchBar").value 
        +"'&StatTrack="+document.getElementById("StatTrack").checked
        +"&Sticker="+document.getElementById("Sticker").checked
        +"&Souvenir="+document.getElementById("Souvenir").checked
        +"&Graffiti="+document.getElementById("Graffiti").checked
        +"&Consumer="+document.getElementById("Consumer").checked
        +"&MilSpec="+document.getElementById("MilSpec").checked
        +"&Industrial="+document.getElementById("Industrial").checked
        +"&Restricted="+document.getElementById("Restricted").checked
        +"&Classified="+document.getElementById("Classified").checked
        +"&Covert="+document.getElementById("Covert").checked
        +"&Base="+document.getElementById("Base").checked
        +"&High="+document.getElementById("High").checked
        +"&Extraordinary="+document.getElementById("Extraordinary").checked
        +"&Remarkable="+document.getElementById("Remarkable").checked
        +"&Exotic="+document.getElementById("Exotic").checked
        +"&Contraband="+document.getElementById("Contraband").checked
        +"&FactoryNew="+document.getElementById("FactoryNew").checked
        +"&MinimalWear="+document.getElementById("MinimalWear").checked
        +"&FieldTested="+document.getElementById("FieldTested").checked
        +"&WellWorn="+document.getElementById("WellWorn").checked
        +"&BattleScarred="+document.getElementById("BattleScarred").checked
        +"&Sortieren="+document.getElementById("searchForm").elements["Sortieren"].value
        ;
    xhttp.send(data);
}
window.onload = search()
</script>

</body>
</html>
